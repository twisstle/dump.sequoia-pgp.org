{ stdenv, pkgs, llvmPackages, pkgconfig
, libiconv, makeWrapper, fetchFromGitLab
}:

let
  mkRustPlatform = pkgs.callPackage ./mk-rust-platform.nix {};
  rustPlatform = mkRustPlatform {
    date = "2021-09-14";
    channel = "nightly";
  };

in rustPlatform.buildRustPackage rec {
  pname = "sequoia-dump";
  version = "0.1.0";

  src = fetchFromGitLab {
    owner = "sequoia-pgp";
    repo  = "dump.sequoia-pgp.org";
    rev   = "4f1a82ccc11bb9fbdcc74b34fdc1a9e1dca23d2e";
    hash  = "sha256:0w4d7vy6irx3w0dhrgs0q6qw11ngbxnwl9gp2kiyb1d7znx4r39c";
  };

  cargoSha256 = "sha256:1964hz46qjrx1lrp3ymfm9c261fp24c6r62piy544aqvm2xbmka2";
  verifyCargoDeps = true;

  postInstall = ''
    cp -r static $out/
    cp -r templates $out/
  '';

  buildInputs = with pkgs; [
    nettle
  ];

  nativeBuildInputs = with pkgs; [
    llvmPackages.clang
    pkgconfig
    capnproto
  ];

  # compilation of -sys packages requires manually setting LIBCLANG_PATH
  LIBCLANG_PATH = "${pkgs.llvmPackages.libclang.lib}/lib";

  COMMIT_SHA = builtins.substring 0 7 src.rev;

  meta = with stdenv.lib; {
    description = "A PGP packet dumper as a service";
    homepage    = https://dump.sequoia-pgp.org;
    license     = with licenses; [ gpl3 ];
    maintainers = with maintainers; [ puzzlewolf ];
    platforms   = platforms.all;
  };
}

# A PGP packet dumper as a service.

This is the code running on
[https://dump.sequoia-pgp.org](https://dump.sequoia-pgp.org).
Currently, it is nothing more than `sq packet dump` with the rocket
web framework.

use std::io::{self, Write};

use axum::{
    body,
    extract::{Form, FromRef, Path, State},
    http::{header, StatusCode},
    response::{IntoResponse, Response},
    routing::get,
    Router,
};

use axum_template::{engine::Engine, RenderHtml};
use include_dir::{include_dir, Dir};
use tera::Tera;

#[macro_use] extern crate serde_derive;
extern crate serde_json;

extern crate sequoia_openpgp as openpgp;
use openpgp::fmt::hex;
use openpgp::crypto::SessionKey;

mod known_keys;
mod dump;
mod explain_with_colons;

#[derive(Debug, Serialize)]
struct TemplateContext {
    data: String,
    hex: bool,
    mpis: bool,
    session_key: Option<String>,
    appears_encrypted: bool,
    dump: String,
    include_permalink: bool,
    version: String,
    commit: String,
    sequoia_version: String,
    crypto_backend: String,
}

impl Default for TemplateContext {
    fn default() -> Self {
        Self {
            data: "".into(),
            hex: false,
            mpis: false,
            session_key: None,
            appears_encrypted: false,
            dump: "".into(),
            include_permalink: false,
            version: env!("CARGO_PKG_VERSION").to_string(),
            commit: option_env!("COMMIT_SHA")
                .unwrap_or(env!("VERGEN_SHA_SHORT"))
                .to_string(),
            sequoia_version: openpgp::VERSION.to_string(),
	    crypto_backend: openpgp::crypto::backend(),
        }
    }
}

#[derive(Deserialize)]
struct Post {
    data: Option<String>,
    hex: Option<String>,
    mpis: Option<String>,
    session_key: Option<String>,
}

#[axum::debug_handler]
async fn handle(
    State(engine): State<AppEngine>,
    form: Form<Post>)
    -> Result<RenderHtml<std::string::String, Engine<Tera>, TemplateContext>, AppError>
{
    let data = form.data.as_ref();
    let hex = form.hex.as_ref().map(|s| s == "on").unwrap_or_default();
    let mpis = form.mpis.as_ref().map(|s| s == "on").unwrap_or_default();
    let session_key = form.session_key.as_ref()
	.filter(|s| ! s.is_empty()).map(|s| s.as_str());

    let mut context = TemplateContext::default();
    context.hex = hex;
    context.mpis = mpis;

    if let Some(data) = data {
        let sk: Option<SessionKey> = if let Some(mut sk) = session_key {
            if let Some(i) = sk.find(':') {
                sk = &sk[i + 1..];
            }
            Some(from_hex(sk, true)?.into())
        } else {
            None
        };

        let mut result = Vec::new();
        let (sk, encrypted) = match
            dump::dump(&mut io::Cursor::new(data.as_bytes()),
                       &mut result,
                       mpis,
                       hex,
                       sk,
                       known_keys::decrypt_pkesk,
                       |_| None,
                       32 * 4 + 80)
        {
            Ok(dump::Kind::Message { encrypted, session_key }) =>
		(session_key, encrypted),
	    Ok(_) => (None, false),
            Err(e) => {
                if let Ok(dump) = dump_colons(data.as_bytes()) {
                    writeln!(&mut result, "{}", dump)?;
                } else {
                    writeln!(&mut result, "Parsing failed: {}", e)?;
                }
                (None, false)
            },
        };

        context.data = data.clone();
        context.session_key = sk.as_ref().map(|sk| hex::encode(sk));
        context.appears_encrypted = encrypted;
        context.dump = String::from_utf8(result)?;
        context.include_permalink = data.len() < 2000;
    }

    Ok(RenderHtml("index.html.tera".into(), engine, context))
}

/// Serve static files baked into the executable.
async fn static_path(Path(path): Path<String>) -> impl IntoResponse {
    let path = path.trim_start_matches('/');
    let mime_type = mime_guess::from_path(path).first_or_text_plain();

    static STATIC_DIR: Dir<'_> = include_dir!("$CARGO_MANIFEST_DIR/static");
    match STATIC_DIR.get_file(path) {
        None => Response::builder()
            .status(StatusCode::NOT_FOUND)
            .body(body::boxed(body::Empty::new()))
            .unwrap(),
        Some(file) => Response::builder()
            .status(StatusCode::OK)
            .header(
                header::CONTENT_TYPE,
                header::HeaderValue::from_str(mime_type.as_ref()).unwrap(),
            )
            .body(body::boxed(body::Full::from(file.contents())))
            .unwrap(),
    }
}

type AppEngine = Engine<Tera>;

#[derive(Clone, FromRef)]
struct AppState {
    engine: AppEngine,
}

#[tokio::main]
async fn main() {
    eprintln!("dump.sequoia-pgp.org using Sequoia {} ({})",
	      openpgp::VERSION, openpgp::crypto::backend());

    let addrspec = std::env::args().nth(1)
	.unwrap_or_else(|| "0.0.0.0:8000".to_string()).parse().unwrap();

    let tera = Tera::new("templates/**/*").unwrap();

    // build our application with a single route
    let app = Router::new()
	.route("/", get(handle).post(handle))
	.route("/static/*path", get(static_path))
	.with_state(AppState {
            engine: Engine::from(tera),
        });

    axum::Server::bind(&addrspec)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

/// Dumps GnuPG's machine readable interface (`--with-colon`).
fn dump_colons(data: &[u8]) -> openpgp::Result<String> {
    let mut buf = Vec::new();
    let echo = true;
    let mut first = true;
    let mut good = false;

    use io::BufRead;
    let mut reader = io::BufReader::new(data);
    let mut line_buffer = String::new();
    while let Ok(_) = reader.read_line(&mut line_buffer) {
        if line_buffer.is_empty() {
            break;
        }

        if echo {
            if ! first {
                writeln!(&mut buf)?;
            }
            writeln!(&mut buf, "{}", line_buffer.trim_end())?;
            first = false;
        }
        good |= explain_with_colons::explain(&line_buffer, &mut buf)?;
        line_buffer.clear();
    }

    if good {
        Ok(String::from_utf8_lossy(&buf).into())
    } else {
        Err(anyhow::anyhow!("no colon data found"))
    }
}

/// A helpful function for converting a hexadecimal string to binary.
/// This function skips whitespace if `pretty` is set.
pub(crate) fn from_hex(hex: &str, pretty: bool) -> openpgp::Result<Vec<u8>> {
    use openpgp::Error;
    const BAD: u8 = 255u8;
    const X: u8 = 'x' as u8;

    let mut nibbles = hex.as_bytes().iter().filter_map(|x| {
        match *x as char {
            '0' => Some(0u8),
            '1' => Some(1u8),
            '2' => Some(2u8),
            '3' => Some(3u8),
            '4' => Some(4u8),
            '5' => Some(5u8),
            '6' => Some(6u8),
            '7' => Some(7u8),
            '8' => Some(8u8),
            '9' => Some(9u8),
            'a' | 'A' => Some(10u8),
            'b' | 'B' => Some(11u8),
            'c' | 'C' => Some(12u8),
            'd' | 'D' => Some(13u8),
            'e' | 'E' => Some(14u8),
            'f' | 'F' => Some(15u8),
            'x' | 'X' if pretty => Some(X),
            _ if pretty && x.is_ascii_whitespace() => None,
            _ => Some(BAD),
        }
    }).collect::<Vec<u8>>();

    if pretty && nibbles.len() >= 2 && nibbles[0] == 0 && nibbles[1] == X {
        // Drop '0x' prefix.
        nibbles.remove(0);
        nibbles.remove(0);
    }

    if nibbles.iter().any(|&b| b == BAD || b == X) {
        // Not a hex character.
        return
            Err(Error::InvalidArgument("Invalid characters".into()).into());
    }

    // We need an even number of nibbles.
    if nibbles.len() % 2 != 0 {
        return
            Err(Error::InvalidArgument("Odd number of nibbles".into()).into());
    }

    let bytes = nibbles.chunks(2).map(|nibbles| {
        (nibbles[0] << 4) | nibbles[1]
    }).collect::<Vec<u8>>();

    Ok(bytes)
}

// Make our own error that wraps `anyhow::Error`.
struct AppError(anyhow::Error);

// Tell axum how to convert `AppError` into a response.
impl IntoResponse for AppError {
    fn into_response(self) -> Response {
        (
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("Something went wrong: {}", self.0),
        )
            .into_response()
    }
}

// This enables using `?` on functions that return `Result<_, anyhow::Error>` to turn them into
// `Result<_, AppError>`. That way you don't need to do that manually.
impl<E> From<E> for AppError
where
    E: Into<anyhow::Error>,
{
    fn from(err: E) -> Self {
        Self(err.into())
    }
}
